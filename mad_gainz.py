# Are you the Eye of the Tiger? Can you handle The Danger Zone? 
""" Danger Zone - https://www.youtube.com/watch?v=siwpn14IE7E&list=PLCbTklSMKBxCVqFvuU9PajshPkV2GiE25&index=582 
    Eye of the Tiger - https://www.youtube.com/watch?v=btPJPFnesV4 """

def color_print(text: str, effect: str) -> str:
    """
    The function will change the color of text when called.
    After each string is colored and printed, it will reset
        back to default.
    
    :param text: The string text that will change its font color.
    :param effect: Changing the string color.
    """
    output_string = "{0}{1}{2}".format(effect, text, RESET)
    print(output_string)


def banner_text(text=" ", screen_width=60):
    """ 
    This function creates a welcoming banner. The Parameters are for strings and screenwidth.

    :param text: Prints text within banner.
    :param screen_width: Verifies string does not exceed past 60 characters. 
    """
    if len(text) > screen_width - 4:
        raise ValueError("String {0} is larger than specified width {1}"
                        .format(text, screen_width))

    if text == "*":
        print("*" * screen_width)  
    else:
        output_string = "**{0}**".format(text.center(screen_width - 4))
        print(output_string)

RED = '\u001b[31m'
GREEN = '\u001b[32m'
YELLOW = '\u001b[33m'
BLUE = '\u001b[34m'
CYAN = '\u001b[36m'
BOLD = '\u001b[1m'
RESET = '\u001b[0m'

# Print banner
print()
banner_text("*")
banner_text("Welcome to the 2021 Mad Gainz Challenge!")
banner_text("Maybe you've heard of previous Challenges.")
banner_text("Maybe you're back for round two.")
banner_text("If you've never heard of these Challenges,")
banner_text("You gon' learn today.")
banner_text()
banner_text("The Challenge is not associated with Charter")
banner_text("in any shape or form. <- Gotta mention it, ya know")
banner_text()
banner_text("NOW LET'S MAKE THESE GAINZ BABY!")
banner_text("*")
print()

color_print('<->' * 20, CYAN)
color_print("\nHere are a few things about this Challenge: ", YELLOW)
color_print("\n1. This is a 6 month Lifestyle Challenge by adjusting your "
            "\n   eating habits & exercising on a weekly basis.", GREEN)
color_print("\n2. It's a $25 buy in. Total cash will be rewarded to the"
            "\n   top 3 roid...oh uhh, I mean... top 3 winners.", GREEN)
color_print("\n3. This is a group effort to encourage and motivate each other."
            "\n   Total cash won is only an incentive to your progress and"
            "\n   for this reason, refunds will not be issued unless the"
            "\n   participant can complete 100,000 crunches in 60 seconds.", GREEN)
color_print("\n>> Type Yes if you're ready for a change or No if.... well... :( <<", RED)


""" I Command you to Grow! Biceps/Arm Day with CT Fletcher (MOTIVATIONAL) 
    https://www.youtube.com/watch?v=JHiKDa4ip_Q&t=44s"""

make_choice = True
while make_choice:
    gainz_user = input("\nWhat's it gonna be? ").casefold()
    if gainz_user == 'yes':
        make_choice = False
        color_print('\n' + '<->' * 20, CYAN)
        print("\nCT Fletcher has a message for you...")
        color_print("\nI COMMAAAAAAAAND YOU TO GROW."
                    "\nYOU MUST GROW. IM THA BOSS MUTHA TRUCKA..."
                    "\nI TELL YOU WHAT TO DO, YOU DONT TELL ME..."
                    "\nI TOLD YOUR SASS TO GROW!", BOLD + RED)
    elif gainz_user == 'no':
        color_print("\nC'mon, what do you have to lose? Just type yes.", CYAN)
    else:
        color_print("\nDo you even read bruh? Yes is only 3 letters.", YELLOW)    

color_print('\n' + '<->' * 20, YELLOW) 
print()
color_print("\t" * 3 + "HOW IT WORKS", BLUE)
print()
banner_text("*")
banner_text("Sometime between Monday 2/8 and Sunday 2/15")
banner_text("weigh in at 5 Star Nutrition in the Domain.")
banner_text("Tell them you're part of the Bobby Challenge.")
banner_text("Take a picture of the Inbody Scan")
banner_text("and send it to me. The Challenge is")
banner_text("from 2/8/21 - 7/8/21")
banner_text()
banner_text("Points are calulated by 1/10th body fat loss")
banner_text("or 1/10th muscle gained.")
banner_text("You can weigh in to monitor progress every")
banner_text("3 weeks or so but it's the final weigh in that")
banner_text("matters. Its optional to send me the updates.")
banner_text("Lets start off by doing 10-15 push ups")
banner_text("every hour or so.")
banner_text("*")



  



